### FLASK_APP=server.py FLASK_ENV=production FLASK_RUN_HOST=0.0.0.0 FLASK_RUN_PORT=8080 flask run

import sys
from flask import Flask, request, abort
from flask_httpauth import HTTPBasicAuth
from werkzeug.security import generate_password_hash, check_password_hash
import json
import http.client
#from mapping_list import * 
import http.client
import jenkins
import time
import configparser
import requests
from requests.exceptions import HTTPError


app = Flask(__name__) 
auth = HTTPBasicAuth()

users = {
    "sp": generate_password_hash("printerly1234")
}

### load playbook config file
try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser
    abort(500,)

config = ConfigParser()
config.read('./playbook_settings.ini')
setting = ConfigParser()
setting.read('./settings.ini')


def extract_from_content(array, entry, keyword):
    arr = array
    if entry == 'incident':
        return arr['messages'][0][entry][keyword]
    if entry == 'log_entries' and keyword == 'user':
        return arr['messages'][0][entry][0]['agent']['summary']
    else:
        return False


def update_incident_notes(pd_trggier_user, pd_id, alert_title, message='nil'):
    conn = http.client.HTTPSConnection("api.pagerduty.com")
    if message == 'nil':
        message = "{0} trigger custom action for alert \"{1}\"".format(pd_trggier_user, alert_title)

    payload = {
        'note': {
            'content': message
        }
    }

    headers = {
        'accept': "application/vnd.pagerduty+json;version=2",
        'content-type': "application/json",
        'from': "izek.chen@scalablepress.com",
        'authorization': "Token token=xzKbndff_aVFViG-pns-"
    }
    conn.request("POST", "/incidents/{}/notes".format(pd_id), json.dumps(payload), headers)
    res = conn.getresponse()
    print(res.status)
    return res.status
    #data = res.read()
    ### not handling edge case



    ### token 11756447f9b3b9baebd8ef723104a234de


def run_jenkins(alert_title):
    PB_NAME, PB_TYPE, PB_PATH, PB_TOKEN, PB_PARAMETERS = config.get( alert_title, 'PB_NAME')  , config.get( alert_title, 'PB_TYPE'), config.get( alert_title, 'PB_PATH'), config.get( alert_title, 'PB_TOKEN'), config.get( alert_title, 'PB_PARAMETERS')
    print(PB_NAME, PB_TYPE, PB_PATH, PB_TOKEN, PB_PARAMETERS)
    if PB_PARAMETERS:
        JENKINS_URL = setting.get( 'jenkins', 'JENKINS_URL')
        print(JENKINS_URL)
        JOB_URL = '{0}/build?job={1}&token={2}'.format(JENKINS_URL, PB_PATH, PB_TOKEN)
        print(JOB_URL)
        r = requests.post(JOB_URL)
        return r.status_code, JOB_URL, PB_NAME
    else:
        JENKINS_URL = setting.get( 'jenkins', 'JENKINS_URL_PARAMS')
        ### add handler
        print(JENKINS_URL)
        JOB_URL = '{0}/build?job={1}&token={2}'.format(JENKINS_URL, PB_PATH, PB_TOKEN)
        print(JOB_URL)



@auth.verify_password
def verify_password(username, password):
    if username in users and check_password_hash(users.get(username), password):
        return username

@app.route('/custom_action', methods=['POST']) 
@auth.login_required 
def webhook():
    if request.method == 'POST':
        data = request.get_json()
        print(data)

        ### 1. get the alert title and incident id
        alert_title = extract_from_content(data, 'incident', 'title')
        pd_id = extract_from_content(data, 'incident', 'id')
        pd_trggier_user = extract_from_content(data, 'log_entries', 'user')



        ### 2. update incident before trigger action
        update_result = update_incident_notes(pd_trggier_user, pd_id, alert_title)
        print(update_result)

        ### 3. trigger action
        #if config.has_section(alert_title):
        #    job_type = config.get(alert_title, 'PB_TYPE')

        #    if job_type == 'jenkins':
        #        print('run custom action for alert {}'.format(alert_title))
                #PB_NAME, PB_TYPE, PB_PATH, PB_TOKEN = config.get( alert_title, 'PB_NAME') , config.get( alert_title, 'PB_TYPE'), config.get( alert_title, 'PB_PATH'), config.get( alert_title, 'PB_TOKEN')
                #PB_PARAMETERS = 
        #        jenkins_state, JOB_URL, PB_NAME = run_jenkins(alert_title)
        #        print(jenkins_state)
        #        print(type(jenkins_state))

        #        if jenkins_state == 201:
        #            message = 'Trigger job \'{0}\' successfully with code {1}. job url {2}'.format(PB_NAME, jenkins_state, JOB_URL)
        #            print(message)
        #            update_incident_notes(pd_trggier_user, pd_id, alert_title, message)
        #        else:
        #            message = 'Trigger job \'{0}\'failed with code {1}. Job url {2}'.format(PB_NAME, jenkins_state, JOB_URL)
        #            print(message)
        #            update_incident_notes(pd_trggier_user, pd_id, alert_title, message)
        #            abort(jenkins_state, 'trigge job failed with code {}'.format(jenkins_state))

                #output = jenkins_obj.build_job(PB_NAME, PB_PARAMETERS, PB_TOKEN)
                #print ("Jenkins Build URL: {}".format(output['url']))
                #except Exception as e:
                #    print(e)
            #if pb_type == 'jenkins':
            ### JENKINS_URL/job/test/job/izek-custom-action-test/build?token=TOKEN_NAME

        #else:
            ### need to add a update back to incident 
        #    abort(400, 'playbook not exist with this alert')


        ### 4. update result back to incident
            

        return '', 200
    else:
        abort(400, 'something wrong')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
