import sys
from flask import Flask, request, abort
from flask_httpauth import HTTPBasicAuth
from werkzeug.security import generate_password_hash, check_password_hash
import http.client
import json
import jenkins
import configparser
import requests

try:
    from configparser import ConfigParser
except ImportError:
    abort(500,'Load Config failed')

app = Flask(__name__) 
auth = HTTPBasicAuth()

users = {
    "sp": generate_password_hash("printerly1234")
}

class extract_data:
    def get_alert_titile(self, raw_data): 
        arr = raw_data
        if 'title' in arr['messages'][0]['incident']:
            return arr['messages'][0]['incident']['title']
        else:
            return "Title not exist"
    
    def get_pd_id(self, raw_data):
        arr = raw_data
        if 'id' in arr['messages'][0]['incident']:
            return arr['messages'][0]['incident']['id']
        else:
            return "PD ID not exist"

    def get_trigger_user(self, raw_data):
        arr = raw_data
        if arr['messages'][0]['log_entries'][0]['agent']['summary']:
            return arr['messages'][0]['log_entries'][0]['agent']['summary']
        else:
            return "trigger user not found"   

class incident_update:
    def __init__(self):
        self.conn = http.client.HTTPSConnection("api.pagerduty.com")
        self.headers = {
            'accept': "application/vnd.pagerduty+json;version=2",
            'content-type': "application/json",
            'from': "izek.chen@scalablepress.com",
            'authorization': "Token token=xzKbndff_aVFViG-pns-"
        }
    def post_update(self, pd_trggier_user, pd_id, alert_title, message='nil'):
        conn = self.conn
        headers = self.headers

        ### define message
        if message == 'nil':
            message = "{0} trigger custom action for alert \"{1}\"".format(pd_trggier_user, alert_title)

        ### define payload
        payload = {
            'note': {
                'content': message
            }
        }

        ### post update to PD
        conn.request("POST", "/incidents/{}/notes".format(pd_id), json.dumps(payload), headers)
        res = conn.getresponse()
        res.close()

        return res.status if res.status == 201 else 400 

class jenkins_action:
    def __init__(self,config_file, setting_file):
        self.cf = ConfigParser()
        self.cf.read(config_file)
        self.st = ConfigParser()
        self.st.read(setting_file)
    def check_ca_exist(self, alert_title):
        if self.cf.has_section(alert_title):
            return True
        return False
    
    def gathering_ca_details(self, alert_title):
        return self.cf.get( alert_title, 'PB_NAME')  , self.cf.get( alert_title, 'PB_TYPE'), self.cf.get( alert_title, 'PB_PATH'), self.cf.get( alert_title, 'PB_TOKEN'), self.cf.get( alert_title, 'PB_PARAMETERS')

    def parse_param(self, PB_PARAMETERS):
        data = json.loads(PB_PARAMETERS)
        param_string = '&'.join('{}={}'.format(*p) for p in data.items())
        return param_string
        

    def run_jenkins_job(self, PB_NAME, PB_TYPE, PB_PATH, PB_TOKEN, PB_PARAMETERS):
        print(PB_NAME, PB_TYPE, PB_PATH, PB_TOKEN, PB_PARAMETERS)
        if PB_PARAMETERS:
            JENKINS_URL = self.st.get( 'jenkins', 'JENKINS_URL_PARAMS')
            #print(JENKINS_URL)
            params_string = self.parse_param(PB_PARAMETERS)
            JOB_URL = '{0}/build?token={1}&job={2}&{3}'.format(JENKINS_URL, PB_TOKEN, PB_PATH, params_string )
            print("run custom action with URL: %s" % JOB_URL) 
            r = requests.post(JOB_URL) 
            print(r.status_code)           
            return r.status_code, JOB_URL #201, 404, 403
    
            
        else:
            print("No Parameters exist")   
            JENKINS_URL = self.st.get( 'jenkins', 'JENKINS_URL')  
            JOB_URL = '{0}/build?job={1}&token={2}'.format(JENKINS_URL, PB_PATH, PB_TOKEN)
            print("run custom action with URL: %s" % JOB_URL)
            r = requests.post(JOB_URL)
            print(r.status_code)
            return r.status_code, JOB_URL  #201, 404, 403

    def run(self, alert_title):
        print("run job for %s" % alert_title)
        PB_NAME, PB_TYPE, PB_PATH, PB_TOKEN, PB_PARAMETERS = self.gathering_ca_details(alert_title)
        if PB_TYPE == 'jenkins':      
            response_code, JOB_URL = self.run_jenkins_job(PB_NAME, PB_TYPE, PB_PATH, PB_TOKEN, PB_PARAMETERS)            
            return response_code, JOB_URL
        elif PB_TYPE == 'Ansible':
            print("TBD")
        else:
            print(" Custom Action type %s not exist" % PB_TYPE)

    

@auth.verify_password
def verify_password(username, password):
    if username in users and check_password_hash(users.get(username), password):
        return username

@app.route('/custom_action', methods=['POST']) 
@auth.login_required 
def webhook():
    if request.method == 'POST':
        data = request.get_json()
        #print(data)
        get_data = extract_data()

        ### get alert_title, PD_id and trigger user name
        alert_title = get_data.get_alert_titile(data)
        pd_id = get_data.get_pd_id(data)
        pd_trggier_user = get_data.get_trigger_user(data)

        print("user '%s' trigger custom action for alert '%s', PD id '%s'" % (pd_trggier_user,alert_title,pd_id))

        conn = incident_update()
        post_result = conn.post_update(pd_trggier_user, pd_id, alert_title)
        print(post_result)
        if post_result != 201:
            abort(400, 'Post incident update to PD failed')

        ### check custom action exist or not
        jks_action =  jenkins_action('./playbook_settings.ini', './settings.ini')

        if jks_action.check_ca_exist(alert_title):
            print('Custom Action exist, check the action type and gathering related detils')
            response_code, JOB_URL = jks_action.run(alert_title)
            conn.post_update(pd_trggier_user, pd_id, alert_title, "JOB URL: %s" % JOB_URL)
            if response_code != 201:
                conn.post_update(pd_trggier_user, pd_id, alert_title, "Run Job failed with status code %s" % response_code)
                abort(400)
            return 'run custom action successfully', 200
              
        else:
            conn.post_update(pd_trggier_user, pd_id, alert_title,'No Custom Action Available')
            abort(400, 'No playbook available')
        

        #return '', 200
    else:
        abort(400, 'something wrong')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)